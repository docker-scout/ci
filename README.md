# Docker Scout Gitlab CI/CD Component

This project is a CI/CD component to enable the **Docker Scout** container scanning in your projects.

## What is Docker Scout

Docker Scout is a solution for proactively enhancing your software supply chain security. It works by analyzing your container images, which consist of layers and software packages. These components are susceptible to vulnerabilities that can compromise the security of containers and applications.

Docker Scout compiles an inventory of these components, also known as a Software Bill of Materials (SBOM). The SBOM is then matched against a continuously updated vulnerability database to pinpoint security weaknesses.

Docker Scout is a standalone service and platform that you can interact with using Docker Desktop, Docker Hub, the Docker CLI, and the Docker Scout Dashboard. It also facilitates integrations with third-party systems, such as container registries and CI platforms.

For more information, you can refer to the [Docker Scout documentation](https://docs.docker.com/scout/).

## Usage

The Docker Scout CI/CD component generates a JSON report that is compliant with the [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/) of **GitLab Ultimate**. That means the results of the Docker Scout scans are perfectly integrated into the GitLab Vulnerability Report, like the other scan results (SAST, DAST,...), and you benefit from the information being displayed directly in the merge request.

### Setup

Add the following snippet to your Gitlab CI/CD pipeline to enable Docker Scout in your project.

```yaml
include: 
  - component: gitlab.com/docker-scout/ci/cves@main
    inputs:
      stage: test
      options: --only-severity=critical,high,medium --epss

docker-scout-cves:      
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG 
```

**Remark**: If you use the Docker Scout CI/CD component with GitLab Free or Premium, you don't benefit from the GitLab Ultimate integration, but you can still download reports from the CI job artifacts.  Update the `docker-scout-cves` section like in the below example to make the **Job Artifact widget** available on the output page of the CI job:

```yaml
docker-scout-cves:      
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG 
  artifacts:
    paths:
      - ds-container-scanning-report.json 
```

### Authentication to Docker Hub

Add the following environment variables to the CI/CD Variables of your project or group:
- `DOCKER_HUB_USER`, this is your Docker Handle on Docker Hub.
- `DOCKER_HUB_PAT`, this is  a personal access token (PAT) to use as an alternative to your password for [Docker CLI authentication](https://docs.docker.com/security/for-developers/access-tokens/).

### Example of CI/CD pipeline

> `.gitlab-ci.yml`:
```yaml
include: 
  - component: gitlab.com/docker-scout/ci/cves@main
    inputs:
      stage: test
      options: --only-severity=critical,high,medium --epss

docker-scout-cves:      
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG      
 
docker-build:
  # Use the official docker image.
  image: docker:cli
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$DOCKER_IMAGE_NAME" .
    - docker push "$DOCKER_IMAGE_NAME"
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        docker tag "$DOCKER_IMAGE_NAME" "$CI_REGISTRY_IMAGE:latest"
        docker push "$CI_REGISTRY_IMAGE:latest"
      fi
  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile        
```
